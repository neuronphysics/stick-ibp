from libcpp.vector cimport vector
import numpy as np
cimport numpy as np
#Based on this thread :https://groups.google.com/forum/#!topic/cython-users/0ouYUUa60R4
ctypedef double (* func_t)(double, double, long)

cdef class wrapper:
     cdef func_t wrapped
     

cdef void initial(int *ns, int *m, double *emax, double* x, double* hx, double* hpx, int *lb, double *xlb, int *ub, double *xub, int* ifault, int* iwv, double* rwv)
cdef void sample(int* iwv, double* rwv, func_t f, func_t fprimax, double* power, long* cnt, double* beta, int* ifault)

cdef void spl1(int *ns, int *n, int *ilow, int *ihigh, int* ipt, double* scum, double *cu, double* x, double* hx, double* hpx, double* z, double* huz, double *huzmax, int *lb, double *xlb, double *hulb, int *ub, double *xub, double *huub, func_t f, func_t fprimax, double* power, long* cnt, double* beta, int* ifault, double *emax, double *eps, double *alcu)

cdef void splhull(double *u2, int* ipt, int *ilow, int *lb, double *xlb, double *hulb, double *huzmax, double *alcu, double* x, double* hx, double* hpx, double* z, double* huz, double* scum, double *eps, double *emax, double* beta, int *i, int *j)

cdef void intersection(double *x1, double *y1, double *yp1, double *x2, double *y2, double *yp2, double *z1, double *hz1, double *eps, int* ifault)

cdef void update(int *n, int *ilow, int *ihigh, int* ipt, double* scum, double *cu, double* x, double* hx, double* hpx, double* z, double* huz, double *huzmax, double *emax, int *lb, double *xlb, double *hulb, int *ub, double *xub, double *huub, int* ifault, double *eps, double *alcu)

cdef double expon(double *x, double *emax)

cdef double muPrevARS(double x, double mk, long N)

cdef double primeMuPrevARS(double x, double mk, long N)

cdef double muARS(double x, double alpha, long N)


cdef double primeMuARS(double x, double alpha, long N)

cdef double Gamma(double x, double alpha, long beta)
cdef double GammaPrima(double x, double alpha, long beta)
cdef wrapper make_wrapper(func_t f)

