import numpy as np
cimport numpy as np
cimport cython
from libc.stdlib cimport malloc, free
from libc.math cimport log, exp, pow, acos, fabs
from cython_gsl cimport *
import ctypes
from cython.parallel import parallel, prange
#################################
from libc.string cimport memset
from libcpp.string cimport string
from cpython cimport array
from libcpp.vector cimport vector
###########################################################
from libc.stdio cimport FILE, fopen, fread, stdout, fclose, fprintf, stderr, sprintf 
###########################################################
from cython.operator cimport dereference as deref
from libcpp cimport bool
import logging
def __getLogger():
    logging.basicConfig(format='%(asctime)s - %(message)s')
    return logging.getLogger(__name__)

__logger = __getLogger()


cdef extern from "f2pyptr.h": 
     void *f2py_pointer(object) except NULL
     
import scipy.linalg.lapack 
import scipy.linalg.blas
##################################
ctypedef np.float64_t REAL_t
cdef extern from "numpy/arrayobject.h":
     cdef bint PyArray_IS_F_CONTIGUOUS(np.ndarray) nogil
     
ctypedef void (*dgetri_ptr) (long *N, double *A, long *LDA, int *IPIV, double *WORK, long *LWORK, int *INFO) nogil
cdef dgetri_ptr dgetri = <dgetri_ptr>f2py_pointer(scipy.linalg.lapack.dgetri._cpointer) 
#DGETRI computes the inverse of a matrix using the LU factorization

ctypedef void (*dgetrf_ptr) (long *M, long *N, double *A, long *LDA, int *IPIV, int *INFO) nogil
cdef dgetrf_ptr dgetrf = <dgetrf_ptr>f2py_pointer(scipy.linalg.lapack.dgetrf._cpointer) 
#DGETRF computes an LU factorization of a general M-by-N matrix A
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(False)
cdef void inverse_matrix(REAL_t* A, long n, int info):
     #Computes the inverse of an LU-factored general matrix using sgetri routine.
     #This routine computes the inverse inv(A) of a general matrix A. Before calling this routine, call sgetrf to factorize A
     cdef np.ndarray[int, ndim=1] ipiv   = np.empty(n, dtype=np.int32)
     cdef np.ndarray[double, ndim=1] work = np.empty(n, dtype=np.float64)
     dgetrf(&n, &n, A, &n, &ipiv[0], &info)
     dgetri(&n, A, &n, &ipiv[0], &work[0], &n, &info)



@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(False)
cdef np.ndarray[ndim=2, dtype=REAL_t] inverse(np.ndarray[ndim=2, dtype=REAL_t] A):
     #Computes the inverse of a suared matrix
     if (A.shape[0] !=A.shape[1]):
        raise TypeError("The input array must be a square matrix")
     cdef long N = A.shape[0]
     cdef int info
     if (A.flags['F_CONTIGUOUS']==False):
         A= np.asfortranarray(A)
          
     inverse_matrix(&A[0,0], N, info)
      
     if info < 0:
        raise ValueError('illegal value in %d-th argument of internal getrf' % -info)
     elif info > 0:
        raise ValueError("Diagonal number %d is exactly zero. Singular matrix." % -info)
     return A

            
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(False)
cdef REAL_t det_sqr_mtx(np.ndarray[ndim=2, dtype=REAL_t] A):
     #Computes the LU-factored general matrix using dgetri routine.
     #This routine computes the determinant of a general matrix A by calling dgetrf to factorize A 
      if (A.shape[0] !=A.shape[1]):
         raise TypeError("The input array must be a square matrix")
     
      cdef long N = A.shape[0]
      
      cdef np.ndarray[int, ndim=1] ipiv = np.asfortranarray(np.zeros(N, dtype=np.int32))
      
      cdef int info=0
      cdef long i
      if (A.flags['F_CONTIGUOUS']==False):
          A= np.asfortranarray(A)
         
      dgetrf(&N, &N, &A[0,0], &N, &ipiv[0], &info)
      
      if info < 0:
          raise ValueError("Det sqr mtx: piviot parameter has an illegal value.")
      elif info > 0:
          raise ValueError("Det sqr mtx: u matrix is singular." )
      
      
      cdef double det = 1.0
      
      for i from 0 <= i < N:
          det *= A[i,i]
          if (ipiv[i] != (i + 1)):
             det*= -1.          
            
      return det

ctypedef int dpotrf_t(char *uplo, long *n, double *a, long *lda, int *info)    
cdef dpotrf_t *dpotrf = <dpotrf_t*>f2py_pointer(scipy.linalg.lapack.dpotrf._cpointer) 
#DPOTRF computes the Cholesky factorization of a real symmetric
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
cpdef np.ndarray[ndim=2, dtype=REAL_t] cholesky( np.ndarray[ndim=2, dtype=REAL_t] hvar):
      #Using lapack library for computing the Cholesky factorization of a real symmetric
      if (hvar.flags['F_CONTIGUOUS']==False):
          hvar= np.asfortranarray(hvar)
      cdef long n = hvar.shape[0]
      cdef int info = 0
      dpotrf('L', &n, &hvar[0, 0], &n, &info)
      if info < 0:
          raise ValueError("Det sqr mtx: piviot parameter has an illegal value.")
      elif info > 0:
          raise ValueError("Det sqr mtx: u matrix is singular." )
      return hvar
 
ctypedef void (*dgemm_ptr) (char *TRANSA, char *TRANSB, long *M, long *N, long *K, double *ALPHA, double *A, long *LDA, double *B, long *LDB, double *BETA, double *C, long *LDC) nogil  
cdef dgemm_ptr dgemm = <dgemm_ptr>f2py_pointer(scipy.linalg.blas.dgemm._cpointer) 
#DGEMM  performs one of the matrix-matrix operations
#     C := alpha*op( A )*op( B ) + beta*C,
@cython.wraparound(False)
@cython.boundscheck(False)
@cython.cdivision(True)
cdef void dot_product(np.ndarray[ndim=2, dtype=REAL_t] a, 
                      np.ndarray[ndim=2, dtype=REAL_t] b, 
                      np.ndarray[ndim=2, dtype=REAL_t] c,  
                      REAL_t ALPHA = 1.0, 
                      REAL_t BETA  = 0.0,
                      char* TRANSA = 'N', 
                      char* TRANSB = 'N'):
      #matrix operations including matrix-matrix multiplication
      #C = alpha* A * B  + beta* C 
      if (a.flags['F_CONTIGUOUS']==False):
          a = np.asfortranarray(a)
          
      if (b.flags['F_CONTIGUOUS']==False):
          b = np.asfortranarray(b)
          
      if (c.flags['F_CONTIGUOUS']==False):
          c = np.asfortranarray(c)
          
      if (c.shape[0] != a.shape[0]) :
           raise TypeError("The numbers of rows of output matrix c must be equal to number of rows in matrix a")
    
      if (c.shape[1] != b.shape[1]):
           raise TypeError("The numbers of columns of output matrix c must be equal to number of columns in matrix b")

      cdef long M, N, K, LDA, LDB, LDC
      cdef char* cN = 'N'
      cdef char* cT = 'T'
      if (a.shape[1] !=b.shape[0]):
         raise TypeError("The numbers of columns in matrix a must be equal to number of rows in matrix b")
      if (TRANSA   == cN):
         M = a.shape[0] #the number  of rows  of the  matrix op( A )  and of the  matrix  C
      elif (TRANSA == cT):
         M = a.shape[1]
      if (TRANSB   == cN):
         N = b.shape[1]
      elif (TRANSB == cT):
         N = b.shape[0]
      K   = a.shape[1]
      LDA = a.shape[0]
      LDB = b.shape[0]
      LDC = a.shape[0]
      dgemm(TRANSA, TRANSB, &M, &N, &K, &ALPHA, &a[0,0], &LDA, &b[0,0], &LDB, &BETA, &c[0,0], &LDC)
      return 
############# ARS ###############
#functions imported from ars.pyx code
from ars import NewMuARS
from ars import OldMuARS

from ars cimport muARS
from ars cimport primeMuARS

from ars cimport muPrevARS
from ars cimport primeMuPrevARS
ctypedef np.float_t FTYPE_t
##################################
###########Slice Sampler##########
#functions imported from SliceSampler.pyx code
from SliceSampler cimport wrapper
from SliceSampler cimport make_wrapper
from SliceSampler cimport logUniform 
from SliceSampler cimport log_beta
from SliceSampler cimport logGamma
from SliceSampler import slice_sampler
##################################

cdef extern from "stdlib.h" nogil:         
    long rand()                    
    void srand(int seed)                    

cdef extern from "time.h" nogil:
    ctypedef long time_t
    time_t time(time_t*)

cdef gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937)
srand(time(NULL))
     
cdef extern from "boost/random/mersenne_twister.hpp" namespace "boost::random" nogil:
    # random number generator
    cdef cppclass mt19937:
        #init
        mt19937() nogil
        #attributes

        #methods
        seed(unsigned long)


cdef extern from "rng_wrapper.hpp" nogil:
    # wrapper to distributions ...
    cdef cppclass rng_sampler[result_type]:
        #init
        rng_sampler(mt19937) nogil
        rng_sampler(unsigned long) nogil
        rng_sampler()  nogil
        # methods (gamma and exp are using rate param)
        result_type normal(result_type, result_type) nogil
        result_type gamma(result_type, result_type) nogil
        result_type uniform(result_type, result_type) nogil
        result_type exp(result_type) nogil
        result_type chisq(result_type) nogil

ctypedef mt19937 rng

@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)     
cdef double uniform(double a, double b):
      #uniform distribution
      cdef int seed = rand()
      cdef rng r
      cdef rng_sampler[double] * rng_p = new rng_sampler[double](seed)
      cdef rng_sampler[double] rng = deref(rng_p)
      return rng.uniform( a, b)
    
@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)     
cdef double Gamma(double shape, double scale, bint inverse=True):
      #inverse gamma or gamma distribution 
      cdef int seed = rand()
      cdef rng r
      cdef rng_sampler[double] * rng_p = new rng_sampler[double](seed)
      cdef rng_sampler[double] rng = deref(rng_p)
      if inverse:
         return 1./rng.gamma(shape, 1.0/scale)
      else:
         return rng.gamma(shape, scale)
     
@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)     
cdef double SampleNormal(double mu, double sd):      
      #Gaussian distribution
      cdef int seed = rand()
      cdef rng r
      cdef rng_sampler[double] * rng_p = new rng_sampler[double](seed)
      cdef rng_sampler[double] rng = deref(rng_p)
      return rng.normal( mu, sd)
          
###########################################################  
cdef extern from "<random>" namespace "std":
    cdef cppclass mt19937:
        mt19937()
        mt19937(unsigned int seed)

    cdef cppclass uniform_real_distribution[T]:
        uniform_real_distribution()
        uniform_real_distribution(T a, T b)
        T operator()(mt19937 gen)


cdef extern from "<algorithm>" namespace "std":
     void random_shuffle[RandomAccessIterator](RandomAccessIterator first, RandomAccessIterator last)
     void sort[RandomAccessIterator](RandomAccessIterator first, RandomAccessIterator last)
     void reverse[BidirectionalIterator](BidirectionalIterator first, BidirectionalIterator last)
     

@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)     
cdef REAL_t get_mu_star(np.ndarray[ndim=2, dtype=np.int64_t] Z, np.ndarray[ndim=1, dtype=REAL_t] mu):
     cdef:
         vector[long] m = Z.sum(axis=0)
         REAL_t mu_star
         long i
         long K = Z.shape[1]      
     #m=Z.sum(axis=0)
     mu_star = 1.0
     for i from 0 <= i < K:
         if (m.at(i) > 0):
            mu_star = mu[i]           
     return mu_star


@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)     
cdef np.ndarray[ndim=2, dtype=REAL_t] M(np.ndarray[ndim=2, dtype=np.int64_t] Z, 
                                        REAL_t sigma_a, 
                                        REAL_t sigma_x):
     ###################################
     #purpose:
     #compute M= (ZT.Z+(sigma_x/sigma_a)^2 I)^{-1}
     #Arguments:
     #sigma_x   : standard deviation of the noise on data
     #sigma_a   : standard deviation of the feature
     #Z         : the latent binary feature matrix
     #Inverse M : computed from equation (45) Griffiths & Ghahramani Infinite Latent Feature Models and the Indian Buffet Process
     ######################################
     cdef:
         long K         = Z.shape[1]         
         np.ndarray[ndim=2, dtype=np.int64_t] ZT = Z.transpose() #Z transpose
         np.ndarray[ndim=2, dtype=REAL_t] novera = np.asfortranarray(np.eye( K, dtype=np.float64))
     #M=(Z.T *Z+ (sigma_x/sigma_a)^2 I)^{-1}
     dot_product(a = ZT.astype(np.float64), b = Z.astype(np.float64), c = novera , ALPHA = 1.0, BETA = pow(sigma_x/sigma_a, 2))
     #C = alpha* A * B  + beta* C 
     return inverse(novera)
    
@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)     
cdef np.ndarray[ndim=2, dtype=REAL_t] meanA(np.ndarray[ndim=2, dtype=REAL_t] M, np.ndarray[ndim=2, dtype=np.int64_t] Z, np.ndarray[ndim=2, dtype=REAL_t] X): 
     ###################################
     #purpose:
     #The posterior mean of the feature weights, A, given X and Z is E[A|X,Z]=M^{-1} . ZT . X
     #Bayesian non-parametric latent feature models (Ghahramani, Griffiths & Sollich 2007)
     #Arguments:
     # M:  (Z^T Z+ (sigma_x^2/sigma_a^2) I)^{-1}
     # Z: a feature matrix 
     # X: Observed data
     ###################################
     cdef np.ndarray[ndim=2, dtype=np.int64_t] ZT = Z.transpose() #Z transpose
     cdef np.ndarray[ndim=2, dtype=REAL_t] ZTX    = np.zeros((ZT.shape[0], X.shape[1]), dtype=np.float64, order='F') 
     dot_product(a = ZT.astype(np.float64) , b = X, c = ZTX ) #multiply matrix Z.T & X
     cdef np.ndarray[ndim=2, dtype=REAL_t] A      = np.zeros((M.shape[0], X.shape[1]), dtype=np.float64, order='F')
     dot_product(a = M, b = ZTX, c = A)
     return A


@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)     
cpdef np.ndarray[ndim=2, dtype=REAL_t] covarA( np.ndarray[ndim=2, dtype=REAL_t] M, REAL_t sigma_x):
     ###################################
     #purpose:
     # compute the co-variance of the matrix A
     #Equation (6) of Accelerated Sampling for the Indian Buffet Process  (Finale Doshi-Velez, Zoubin Ghahramani)
     # M:  (Z^T Z+ (sigma_x^2/sigma_a^2) I)^{-1}
     #output: sigma_x^2 M
     ###################################
     cdef np.ndarray[ndim=2, dtype=REAL_t] std= cholesky(pow(sigma_x, 2) * M)
     return std.transpose()

  
@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)     
cdef np.ndarray[ndim=2, dtype=REAL_t] sample_A(np.ndarray[ndim=2, dtype=REAL_t] X,
                                               np.ndarray[ndim=2, dtype=REAL_t] M,
                                               np.ndarray[ndim=2, dtype=np.int64_t] Z,
                                               REAL_t sigma_x):
      ###################################
      #purpose:
      #sample every feature weights
      #A: is a KxD matrix of weights
      ##################################
      cdef long i,j
      cdef long K = Z.shape[1]
      cdef long D = X.shape[1]
      cdef long N = X.shape[0]
      cdef np.ndarray[ndim=2, dtype=REAL_t] Data, mean_A, std_dev_A
      cdef np.ndarray[ndim=2, dtype=REAL_t] A = np.zeros((K, D), dtype=np.float64, order='F') 
      cdef np.ndarray[ndim=2, dtype=REAL_t] rnd = np.zeros((K, 1), dtype=np.float64, order='F') 
      cdef np.ndarray[ndim=2, dtype=REAL_t] tmp 
      std_dev_A = covarA(M, sigma_x)
      for i from 0 <= i < D:
          Data      = X[:,i].reshape(N, 1)
          tmp = np.zeros((K, 1), dtype=np.float64, order='F')
          mean_A    = meanA(M, Z, Data)
          for j from 0 <= j < K:
              rnd[j,0] =SampleNormal(mean_A[j,0],1.)

          dot_product(std_dev_A, rnd, tmp) 
          for j from 0 <= j < K:
              A[j, i]=tmp[j,0]
      return A
    
@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)  
cdef double mu_new_features_update( double mu_prev, double alpha, long N):
     ########################################
     #purpose:  
     #stick-length for new features k 
     #equation 25 Teh et al. 2007 (Stick-breaking construction for the IBP)
     #using ARS to draw samples
     ########################################
     cdef:
         double mu
         int ns = 100
         int m  = 2
         double emax= 64
         np.ndarray[FTYPE_t, ndim=1, mode='c'] x   = np.zeros(2, float)
         np.ndarray[FTYPE_t, ndim=1, mode='c'] hx  = np.zeros(2, float)
         np.ndarray[FTYPE_t, ndim=1, mode='c'] hpx = np.zeros(2, float)
         
     x[0]   = 0.1
     x[1]   = 0.98
     hx[0]  = muARS(x[0], alpha, N)
     hx[1]  = muARS(x[1], alpha, N)
     hpx[0] = primeMuARS(x[0], alpha, N)
     hpx[1] = primeMuARS(x[1], alpha, N)
     while True: 
           mu = NewMuARS(ns, m, emax, x, hx, hpx, 1, alpha, N)[0]
           if ( 0 <= mu <= mu_prev):
              break
     return mu


@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)  
cdef np.ndarray[ndim=1, dtype=REAL_t] mu_old_features_update( np.ndarray[ndim=1, dtype=REAL_t] mu, 
                                                             np.ndarray[ndim=2, dtype=np.int64_t] Z, 
                                                             long K_dagger, 
                                                             long N):
     ########################################
     #purpose:  
     #Update mu(k) for k=1,...,K_dagger-1 from the conditional probability of mu(k)
     #equation 28 Teh et al. 2007 (Stick-breaking construction for the IBP)
     #Arguments:
     #Z: The binary features
     #mu feature presence probability
     ########################################
     cdef:
         long i
         long K = Z.shape[1]
         int ns = 100
         int m  = 2
         double emax= 64.
         np.ndarray[FTYPE_t, ndim=1, mode='c'] x   = np.zeros(2, float)
         np.ndarray[FTYPE_t, ndim=1, mode='c'] hx  = np.zeros(2, float)
         np.ndarray[FTYPE_t, ndim=1, mode='c'] hpx = np.zeros(2, float)
         double mu_prev, mu_next
         double tmp
         np.ndarray[ndim=1, dtype=REAL_t] mk = Z.sum(axis=0).astype(np.float64)
     #print mk

     x[0]   = 0.05
     x[1]   = 0.99
     for i from 0 <= i < K_dagger:    
         hx[0]  = muPrevARS(x[0], mk[i], N)
         hx[1]  = muPrevARS(x[1], mk[i], N)
         hpx[0] = primeMuPrevARS(x[0], mk[i], N)
         hpx[1] = primeMuPrevARS(x[1], mk[i], N)
         
         if (i > 0):
            mu_prev = mu[i-1]
         else:
            mu_prev = 1.0
         
         mu_next = mu[i+1]
         while True:               
               tmp=OldMuARS(ns, m, emax, x, hx, hpx, 1, mk[i], N)[0] 
               
               if (mu_next<=tmp<=mu_prev):
                   mu[i]= tmp 
                   break
         
     return mu
 

@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)  
cdef double get_new_S(double mu_star):
     ########################################
     #purpose:
     #sample a slice S uniformly between 0, and the stick-length of the last active component (mu*)
     #S: The auxiliary slice variable (eq. 21 Teh et al. 2007)
     ########################################
     cdef:
        wrapper wrap_f  = make_wrapper(logUniform)
        vector[double] S
     S = slice_sampler(1,
                  wrap_f, 
                  a= mu_star,
                  m = 0,
                  p= 1,
                  x0_start=0.0, 
                  adapt_w=False, 
                  w_start=.2,
                  interval_method ='doubling')

     return S[0] 

    
@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)  
cdef double sampleSigma(double sigma_hyper_a, double sigma_hyper_b, np.ndarray[ndim=2, dtype=REAL_t] G):
     ###################################
     #purpose:Learning the noise level and Inferring the scale of the data.
     # Infinite Sparse Factor Analysis and Infinite Independent Components Analysis Knowles & Ghahramani 
     #This function can be used to sample noise (equations 18 & 19)  
     ###################################
     cdef:
         long row    = G.shape[0]
         long column = G.shape[1]
         double posterior_shape = sigma_hyper_a + 0.5 * row * column
         double var, posterior_scale
         np.ndarray[ndim=2, dtype=REAL_t] mult
     if (row >= column):
         mult=np.zeros((G.shape[1],G.shape[1]), dtype=np.float64, order='F')
         dot_product(a = np.transpose(G), b = G, c = mult)
     else:
         mult=np.zeros((G.shape[0],G.shape[0]), dtype=np.float64, order='F')
         dot_product(a = G, b = np.transpose(G), c = mult)
     var = np.trace(mult)
     posterior_scale = 1./(sigma_hyper_b + var*0.5)
     return Gamma(posterior_shape, posterior_scale, inverse=True )

@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)  
cdef double sampleAlpha(double alpha_hyper_a, double alpha_hyper_b, long K, long N):
     ###################################
     #purpose:
     # Infer the IBP strength parameter alpha 
     # Infinite Sparse Factor Analysis and Infinite Independent Components Analysis Knowles & Ghahramani
     #http://mlg.eng.cam.ac.uk/zoubin/papers/ica07knowles.pdf
     #This function can be used to sample the IBP strength parameter alpha (equations 20)  
     ###################################
     cdef double posterior_shape = alpha_hyper_a + <double>K
     cdef double H_N = 0.
     cdef long i
     for i from 1 <= i <= N:
          H_N +=1./<double>i 
     cdef double posterior_scale = 1.0 / (alpha_hyper_b + H_N)
     cdef double alpha=Gamma(posterior_shape, posterior_scale, inverse = False )

     return  alpha
  

@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)  
cdef double UnCollapsedLikelihood(np.ndarray[ndim=2, dtype=REAL_t] X, 
                                  np.ndarray[ndim=2, dtype=np.int64_t] Z, 
                                  np.ndarray[ndim=2, dtype=REAL_t] A, 
                                  double sigma_x):
     ##################################
     #purpose: 
     #The uncollapsed likelihood for the infinite Linear-Gaussian model:X=ZA+E
     ##based on equation 42 Thomas Griffiths & Zoubin Ghahramani Infinite latent feature models and Indian buffet Process
     #σ_x^2: the noise variance
     #Z: the feature-assignment matrix
     #K: the dishes in an infinite buffet or the number of nonzero columns in Z
     #X: the observed data
     ##################################
     cdef np.int64_t N= X.shape[0]
     cdef np.int64_t D= X.shape[1]
     cdef np.int64_t K= Z.shape[1]
     
     cdef np.ndarray[ndim=2, dtype=REAL_t] Q= np.copy(X)
     dot_product(a = Z.astype(np.float64), b = A, c = Q, ALPHA = -1.0, BETA = 1.0)
     cdef np.ndarray[ndim=2, dtype=REAL_t] QT= np.transpose(Q)
     cdef:
          double trace
          double pi = acos(-1)
          double ND =<double>N * <double>D
          
     cdef np.ndarray[ndim=2, dtype=REAL_t] log_likelihood
     cdef np.int64_t row    = Q.shape[0]
     cdef np.int64_t column = Q.shape[1]
     if row > column:
        log_likelihood = np.zeros((QT.shape[0],Q.shape[1]), dtype=np.float64, order='F')
        dot_product(a = QT, b = Q, c = log_likelihood)
     else:
        log_likelihood = np.zeros((Q.shape[0],QT.shape[1]), dtype=np.float64, order='F')  
        dot_product(a = Q, b = QT, c = log_likelihood)
     trace= np.trace(log_likelihood)*(-0.5/pow(sigma_x,2))
     
     cdef double denumerator= log(2*pi)*( ND / 2.)+ log(sigma_x)* ND
     return (trace - denumerator )     
     

@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)  
cdef np.ndarray[ndim=2, dtype=np.int64_t] initialize_Z(np.ndarray[ndim=1, dtype=REAL_t] mu, long N):
     ##################################
     #purpose: 
     #Init latent features Z according to IBP(alpha)
     #
     #Equation 1b of Stick-breaking Construction for the indian buffet process
     ##################################
     cdef np.ndarray[ndim=2, dtype=np.int64_t] Z = np.zeros((N,len(mu)), dtype=np.int64, order='F')
     cdef long i,j 
     cdef long K=len(mu)
     cdef double mu_i
     for i from 0 <= i < K:
         mu_i=mu[i]
         for j from 0 <= j < N:
             seed   = rand()
             gsl_rng_set(r, seed)
             Z[j,i] = gsl_ran_bernoulli(r, mu_i)
     return Z 
 
@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)  
cdef np.ndarray[ndim=1, dtype=REAL_t] initialize_mu(double alpha, long K):
     ##################################
     #purpose: 
     #
     #initialize the feature presense probability mu
     #Equation 5 of Stick-breaking Construction for the indian buffet process
     ##################################
     cdef np.ndarray[ndim=1, dtype=REAL_t] mu = np.zeros(K, dtype=np.float64, order='F')
     cdef long  i, seed
     for i from 0 <= i < K:
         seed   = rand()
         gsl_rng_set(r, seed)
         if i >= 1:
            mu[i]= mu[i-1]*gsl_ran_beta(r, alpha, 1)
         else:
            mu[i] = gsl_ran_beta(r, alpha, 1)
     return mu
 

 
cdef class UnCollapseSampler:
    
     cdef object alpha_hyper_param
     cdef object sigma_a_hyper_param
     cdef object sigma_x_hyper_param
     cdef long K, N, D
     cdef double alpha, sigma_a, sigma_x 
     cdef np.ndarray data
     cdef np.ndarray _mu
     cdef np.ndarray _Z
     cdef np.ndarray _A, _M,mean_A
     def __cinit__(self, np.ndarray[ndim=2, dtype=REAL_t] inputs,
                   double alpha_hyper_a,  
                   double alpha_hyper_b,             
                   double sigma_a_hyper_a, 
                   double sigma_a_hyper_b,             
                   double sigma_x_hyper_a, 
                   double sigma_x_hyper_b, 
                   double alpha   = 1.0, 
                   double sigma_a = 1.0,
                   double sigma_x = 1.0):
         
         if ((alpha_hyper_a != None ) and (alpha_hyper_b != None)):
            self.alpha_hyper_param=[alpha_hyper_a, alpha_hyper_b]            
            
         if ((sigma_a_hyper_a != None) and (sigma_a_hyper_b != None)):
            self.sigma_a_hyper_param=[sigma_a_hyper_a, sigma_a_hyper_b] 
            
         if ((sigma_x_hyper_a != None) and (sigma_x_hyper_b != None)):
            self.sigma_x_hyper_param=[sigma_x_hyper_a, sigma_x_hyper_b]
            
         self.N         = inputs.shape[0]
         self.D         = inputs.shape[1]
         self.data      = np.asfortranarray(inputs)
                
         self.alpha     = alpha
         self.sigma_a   = sigma_a
         self.sigma_x   = sigma_x
         __logger.info('Initialize the prior probability whether a feature is present in an object or not, plus the feature assignment matrix .')

         self._mu       = initialize_mu(self.alpha, 1)
         self.K         = len(self._mu)
         self._Z        = initialize_Z(self._mu, self.N)
         #print self._Z
         assert(self._Z.max() == 1 or self._Z.min() == 0)
         __logger.info('compute M = (Z^T.Z+ (sigma_x^2/sigma_a^2) I)^{-1}')
         self._M        = M( self._Z, self.sigma_a, self.sigma_x)
         __logger.info('compute the mean of A which is KxD matrix of weights')
         self._A        = meanA(self._M, self._Z, self.data)
         print self.K 
         
         assert(self._A.shape[0]==self.K and self._A.shape[1]==self.D)
         self.mean_A  = meanA(self._M, self._Z, self.data)

     @cython.cdivision(True)     
     @cython.boundscheck(False)
     @cython.wraparound(False)     
     cpdef void sampleZ(self, long objIndx, long K_star):
           ###################################
           #purpose:
           # Update Z based on equation 26 in Teh et al. 2007
           ###################################
           cdef long j
           cdef vector[long] featIndx
           cdef double ratio_mu, log_prob_z1, log_prob_z0
           cdef double prob_z1, prob_z0, p_z 
           cdef np.ndarray[ndim=2, dtype=np.int64_t] zi
           cdef np.ndarray[ndim=2, dtype=REAL_t] xi = self.data[objIndx, :].reshape(1,self.D)
           for j from 0 <= j < K_star:
               featIndx.push_back(j)
           random_shuffle(featIndx.begin(), featIndx.end())
          
           cdef double mu_star    = get_mu_star(self._Z, self._mu)
          
           for j from 0 <= j < K_star:
               ratio_mu    = self._mu[featIndx.at(j)]/mu_star
               log_prob_z1 = log(ratio_mu)
               log_prob_z0 = log(1.-ratio_mu)   
              
               #compute the log-likelihood when Znk=1
               self._Z[objIndx, featIndx.at(j)] = 1
               zi       = self._Z[objIndx, :].reshape(1,self.K)
               self._M  = M( self._Z, self.sigma_a, self.sigma_x)
               self._A  = meanA(self._M, self._Z, self.data)
               prob_z1  = UnCollapsedLikelihood(xi, zi, self._A, self.sigma_x)
               prob_z1 += log_prob_z1 
              
               # compute the log likelihood when Znk=0
               self._Z[objIndx, featIndx.at(j)] = 0   
               zi       = self._Z[objIndx, :].reshape(1,self.K)
               self._M  = M( self._Z, self.sigma_a, self.sigma_x)
               self._A  = meanA(self._M, self._Z, self.data)
               prob_z0  = UnCollapsedLikelihood(xi, zi, self._A, self.sigma_x)
                                
               #Adding priors (equation 26 of Teh et al. 2007)
               prob_z0 += log_prob_z0
              
               p_z = 1./ (1. + exp(prob_z0 - prob_z1))
               if (uniform(0.,1.)< p_z):
                  self._Z[objIndx, featIndx.at(j)] = 1 
               else:
                  self._Z[objIndx, featIndx.at(j)] = 0 
           return 
    
     @cython.cdivision(True)     
     @cython.boundscheck(False)
     @cython.wraparound(False)     
     cpdef void sample(self, long nstep = 200, int thin = 10):
           ###################################
           #purpose:
           #
           #sample parameters of the linear-Gaussian infinite latent feature model
           ###################################
           assert((self._Z.shape[0] == self.N) and (self._Z.shape[1] == self.K))
           assert((self._A.shape[0] == self.K) and (self._A.shape[1] == self.D))          
           cdef long i, j, K_dagger, seed, iters
           cdef REAL_t mu_star, slice_sample, mu_prev, inactive_features, 
           cdef np.ndarray[ndim=2, dtype=np.int64_t] new_features = np.zeros((self.N,1), dtype=np.int64, order='F')
           cdef str fname_hyper_params, header, count     
           cdef np.ndarray[ndim=2, dtype=REAL_t] G 
            
           cdef int thinner = 1 #counter for thinning parameter 
           cdef long num = 0
           cdef vector[long] objIndx           
           cdef object handle_hyper_params
           cdef np.ndarray[np.uint8_t,cast=True] mask
           if ((len(self.alpha_hyper_param ) >0) or (len(self.sigma_a_hyper_param ) >0) or (len(self.sigma_x_hyper_param ) >0)):
              fname_hyper_params="./sampling_results/sampling_IBP_hyper_params.txt"  
              handle_hyper_params = open(fname_hyper_params, "a")
              header='#'
              if (len(self.alpha_hyper_param ) >0):
                 header+='alpha\t'
              if (len(self.sigma_a_hyper_param ) >0):
                 header+='sigma_a\t'      
              if (len(self.sigma_x_hyper_param ) >0):
                 header+='sigma_x\t'
              header+='\n'
              handle_hyper_params.write(header)
           for i from 0<= i < self.N:
               objIndx.push_back(i)
                          
           for iters from 0<= iters <nstep:
               # sample every object
               random_shuffle(objIndx.begin(), objIndx.end())
               mu_star      = get_mu_star(self._Z, self._mu)
               slice_sample = get_new_S(mu_star)
               while True:
                     mu_prev           = self._mu[self.K-1]
                     inactive_features = mu_new_features_update( mu_prev, self.alpha, self.N)
                     
                     if (inactive_features > slice_sample):
                         self._mu = np.append(self._mu, inactive_features)
                         #Add an empty column for new features
                         self._Z  = np.hstack((self._Z, new_features))
                         self._M  = M( self._Z, self.sigma_a, self.sigma_x)
                         self._A  = sample_A(self.data, self._M, self._Z, self.sigma_x)
                         #generate random seed and sample parameters for the new represented features from the prior
                         self.K +=1
                     else:
                         break
                    
               mask     = ~np.all(self._Z == 0, axis=0)
               K_dagger = np.max(mask)
               print K_dagger,self.K 
               for i from 0 <= i < self.N:
                   #Adding inactive features
                   
                   #for all columns k=1,..,K_dagger do {update features above the slice}
                   self.sampleZ(objIndx.at(i), K_dagger)
               
               mask        = ~np.all(self._Z == 0, axis=0)
               self._mu    = self._mu[mask]
               self._Z     = self._Z[:,mask] 
               self.K      = self._Z.shape[1]
               print mask, self.K
               self._M     = M( self._Z, self.sigma_a, self.sigma_x)    
               self._A     = sample_A(self.data, self._M, self._Z, self.sigma_x)
               self.mean_A = meanA(self._M, self._Z, self.data)
               #update active features of mu_k
               K_dagger = self.K
               
               self._mu = mu_old_features_update(self._mu, self._Z, K_dagger-1, self.N)
               print "Update mu:", self._mu
               if (K_dagger>1):
                  mu_prev        = self._mu[K_dagger-2]
               else:
                   mu_prev       = 1.0
               #Update mu(K_dagger) by sampling from its conditional posterior, eq. 25
               self._mu[K_dagger-1]= mu_new_features_update( mu_prev, self.alpha, self.N)
               print self._Z.sum(axis=0)
               #udating hyper parameters
               if ((len(self.alpha_hyper_param ) > 0) or (len(self.sigma_a_hyper_param ) > 0) or (len(self.sigma_x_hyper_param ) > 0)):
                  
                  hyper_params=""
                  if (len(self.alpha_hyper_param)> 0):
                     
                     self.alpha = sampleAlpha(self.alpha_hyper_param[0], self.alpha_hyper_param[1], self.K, self.N)
                     
                     if (thinner ==thin):
                        #writting alpha into a file
                        hyper_params+="{:.4f}\t".format(self.alpha)
                     
                  if (len(self.sigma_a_hyper_param)> 0):
                     self.sigma_a=sampleSigma(self.sigma_a_hyper_param[0], self.sigma_a_hyper_param[1], self.mean_A)
                     if (thinner ==thin):
                        #writting sigma_a into a file
                        hyper_params+="{:.4f}\t".format(self.sigma_a)
             
                  if (len(self.sigma_x_hyper_param)> 0):
                  
                     G = np.copy(self.data)
                     dot_product(a = self._Z.astype(np.float64), b = self.mean_A, c = G, ALPHA = -1.0, BETA  = 1.0)
                  
                     self.sigma_x=sampleSigma(self.sigma_x_hyper_param[0], self.sigma_x_hyper_param[1], G)
                 
                     if (thinner ==thin):
                        #writting sigma_x into a file
                        hyper_params+="{:.4f}\t".format(self.sigma_x)
                  hyper_params+="\n"
                  handle_hyper_params.write(hyper_params)
               
               if (thinner ==thin):
                  count ="{:d}".format(num)                  
                  #write down all the free parameters of the model into files
                  np.save("./sampling_results/features_Z_"+count, self._Z )
                  np.save("./sampling_results/feature_weights_mu_"+count, self._mu)
                  np.save("./sampling_results/mean_coefficient_A_"+count, self.mean_A)
                  
                  thinner   = 1
                  num      += 1
               else:
                  thinner   += 1
           
           if ((len(self.alpha_hyper_param ) > 0) or (len(self.sigma_a_hyper_param ) > 0) or (len(self.sigma_x_hyper_param ) > 0)):  
              handle_hyper_params.close() 
           
           return 
        
         