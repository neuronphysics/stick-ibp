from libcpp.vector cimport vector
import numpy as np
cimport numpy as np

# define a global name for whatever char type is used in the module
ctypedef double (*func_t)(double, double)

cdef class wrapper:
     cdef func_t wrapped

cdef bint _isinf(double x)
cdef bint _isnan(double x)
cdef double[::1] stepping_out(double x0, double y, double w, int m, func_t f, double arg)

cdef double[::1] doubling(double x0, double y, double w, int p, func_t f, double arg)

cdef bint accept_doubling(double x0, double x1, double y, double w, np.ndarray[ndim=1, dtype=np.float64_t] interv, func_t f, double arg)

cdef double shrinkage(double x0, double y, double w, np.ndarray[ndim=1, dtype=np.float64_t] interv, bint doubling, func_t f, double arg)
cdef double log_beta(double x, double a)
cdef double logUniform(double x, double mu)
cdef double logGamma(double x, double beta)

cdef wrapper make_wrapper(func_t f)
