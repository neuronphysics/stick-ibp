import scipy.io
from sampling import UnCollapseSampler
from matplotlib import pyplot as plt
import numpy as np
from matplotlib.font_manager import FontProperties
import matplotlib
mat_vals = scipy.io.loadmat('block_image_set.mat')
true_weights = mat_vals['trueWeights']
features = mat_vals['features']
data = mat_vals['data']

plt.style.use('ggplot')
fig = plt.figure(facecolor='white',figsize=(20, 20))
font0 = FontProperties()
font = font0.copy()
font.set_family('serif')
fig.suptitle('Raw data', fontsize=35)
plt.imshow(data, cmap='hot', aspect='auto')
matplotlib.rcParams.update({'font.size': 35})
#plt.savefig('raw_data.png', bbox_inches='tight')
plt.show()

data=data[:300,:].copy(order='C')
alpha_hyper_parameter = (1., 1.);
# set up the hyper-parameter for sampling sigma_x
sigma_x_hyper_parameter = (1., 1.);
# set up the hyper-parameter for sampling sigma_a
sigma_a_hyper_parameter = (1., 1.)
ibp = UnCollapseSampler(inputs  = data, 
                        alpha_hyper_a   = alpha_hyper_parameter[0], 
                        alpha_hyper_b   = alpha_hyper_parameter[1], 
                        sigma_a_hyper_a = sigma_a_hyper_parameter[0], 
                        sigma_a_hyper_b = sigma_a_hyper_parameter[1], 
                        sigma_x_hyper_a = sigma_x_hyper_parameter[0], 
                        sigma_x_hyper_b = sigma_x_hyper_parameter[1],
                        alpha   = 0.5, 
                        sigma_a = 0.3, 
                        sigma_x = 0.4 )

print data.shape
ibp.sample( nstep = 500, thin = 20)

