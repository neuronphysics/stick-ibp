from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize
from numpy import get_include
import numpy
import cython_gsl
extra_compile_args = ["-std=c++11", "-ffast-math", "-march=native", "-fopenmp"]
extra_link_args = ['-Wall','-fopenmp']

modules = [
    Extension("matrix", sources=["matrix.pyx"],language="c++",libraries=cython_gsl.get_libraries(),include_dirs=[numpy.get_include(), cython_gsl.get_include()], extra_compile_args=['-std=c++11']),
    Extension("ars", sources=["ars.pyx"], language="c++", include_dirs=[numpy.get_include()], extra_compile_args=extra_compile_args, extra_link_args=extra_link_args),
    Extension("SliceSampler", sources=["SliceSampler.pyx"], language="c++", libraries=["stdc++","gsl", "gslcblas"], include_dirs=[numpy.get_include()], extra_compile_args=extra_compile_args, extra_link_args=extra_link_args),
    Extension("sampling", 
              sources=["sampling.pyx"],
              language="c++",
              extra_compile_args=extra_compile_args,
              extra_link_args=extra_link_args,
              #extra_objects=["/usr/local/LAPACK/liblapack.a"],
              libraries=cython_gsl.get_libraries(),
              include_dirs=[numpy.get_include(), cython_gsl.get_include()],headers=["./rng_wrapper.hpp","./f2pyptr.h"])
]

setup(
    cmdclass={'build_ext': build_ext},
    ext_modules=cythonize(modules)
)
