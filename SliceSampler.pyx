#cython: wraparound=False
#cython: boundscheck=False
#cython: cdivision=True
#cython: nonecheck=False
from cpython cimport array
import cython
import numpy as np
import ctypes
cimport numpy as np
cimport cython
cimport python_unicode

from cython.view cimport memoryview, array  
from libcpp.vector cimport vector
from cython_gsl cimport gsl_rng_set, gsl_sf_gamma
#****************************************************************************************************************************************** 
#************************************************              slice sampling              ************************************************        
#                      Reference: Neal, R.M. (2003). Slice sampling. The Annals of Statistics 31, 705-767.
#****************************************************************************************************************************************** 
#****************************************************************************************************************************************** 
cdef extern from "stdlib.h" nogil:         
    long rand()                    
    void srand(int seed)                    

cdef extern from "time.h" nogil:
    ctypedef long time_t
    time_t time(time_t*)
    
    
cdef extern from "<math.h>"  nogil:
     cdef double floor(double)
     cdef double log(double)
     cdef double exp(double)
     cdef double pow(double, double)
     cdef double tgamma(double) 
     double fabs(double)
     cdef double INFINITY
     cdef double NAN
     
     
cdef extern from "stdint.h"  nogil:
    ctypedef unsigned long long uint64_t  
    

#source-https://github.com/UT-Python-Fall-2013/Class-Projects/blob/3b759f8b92bd141c0eb644661db647e63e09b4a7/pope_project/poyla_sampler.pyx
cdef extern from "gsl/gsl_rng.h" nogil:
     ctypedef struct gsl_rng_type:
        pass
     ctypedef struct gsl_rng:
        pass
     gsl_rng_type *gsl_rng_mt19937
     gsl_rng *gsl_rng_alloc(gsl_rng_type * T)
  
cdef gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937)
srand(time(NULL))
cdef extern from "gsl/gsl_randist.h" nogil:
     double unif "gsl_rng_uniform"(gsl_rng * r)
     double unif_interval "gsl_ran_flat"(gsl_rng * r,double,double)	## syntax; (seed, lower, upper)
     double exponential "gsl_ran_exponential"(gsl_rng * r,double) ## syntax; (seed, mean) ... mean is 1/rate
        


cdef class wrapper:
    
    def __call__(self, v1, v2):
        return self.wrapped(v1,v2)
    def __unsafe_set(self, ptr):
        self.wrapped = <func_t><void *><size_t>ptr   
        
cdef bint _isinf(double x):
    return (fabs(x) == INFINITY)

cdef bint _isnan(double x):
    return (x == NAN)

@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)     
cdef double[::1] stepping_out(double x0, double y, double w, int m, func_t f, double arg):
     """
     Function for finding an interval around the current point 
     using the "stepping out" procedure (Figure 3, Neal (2003))
     Parameters of stepping_out subroutine:
        Input:
            x0 ------------ the current point
             y ------------ logarithm of the vertical level defining the slice
             w ------------ estimate of the typical size of a slice
             m ------------ integer limiting the size of a slice to "m*w"
     (*func_t) ------------ routine to compute g(x) = log(f(x))
       Output:
     interv[2] ------------ the left and right sides of found interval
     """
     cdef int seed = rand()
     #generate random seed
     gsl_rng_set(r, seed)
     cdef double[::1] interv =array((2,), itemsize=sizeof(double), format='d')
     cdef double u
     cdef int J, K
     cdef double g_interv[2]
     #Initial guess for the interval
     u = unif_interval(r,0,1)
     
     interv[0] = x0 - w*u
     interv[1] = interv[0]+ w
     
     #Get numbers of steps tried to left and to right
     if m>0:
        u = unif_interval(r,0,1)
        J = <uint64_t>floor(m*u)
        K = (m-1)-J
     
     #Initial evaluation of g in the left and right limits of the interval 
     g_interv[0]=f(interv[0], arg)
     g_interv[1]=f(interv[1], arg)
     
     #Step to left until leaving the slice 
     while (g_interv[0] > y):
           interv[0] -= w
           g_interv[0]=f(interv[0], arg)
           if m>0:
              J-=1
              if (J<= 0):
                 break
  

     #Step to right until leaving the slice */
     while (g_interv[1] > y):
           interv[1] += w
           g_interv[1]=f(interv[1], arg)
           if m>0:
              K-=1
              if (K<= 0):
                 break
     return interv
     
@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)     
cdef double[::1] doubling(double x0, double y, double w, int p, func_t f, double arg):
     """
     Function for finding an interval around the current point
     using the "doubling" procedure (Figure 4, Neal (2003))
         Input:
            x0 ------------ the current point
             y ------------ logarithm of the vertical level defining the slice
             w ------------ estimate of the typical size of a slice
             p ------------ integer limiting the size of a slice to "2^p*w"
     (*func_t) ------------ routine to compute g(x) = log(f(x))
       Output:
     interv[2] ------------ the left and right sides of found interval    
     """
     cdef int seed = rand()
     #generate random seed
     gsl_rng_set(r, seed)
     cdef double[::1] interv =array((2,), itemsize=sizeof(double), format='d')
     cdef double u
     cdef int K     
     cdef bint now_left
     cdef double g_interv[2]
     #Initial guess for the interval
     u = unif_interval(r,0,1)
     interv[0] = x0 - w*u
     interv[1] = interv[0]+ w
     
     if p>0:
        K = p

     # Initial evaluation of g in the left and right limits of the interval 
     g_interv[0]= f(interv[0], arg)
     g_interv[1]= f(interv[1], arg)

     # Perform doubling until both ends are outside the slice 
     while ((g_interv[0] > y) or (g_interv[1] > y)):
                    u = unif_interval(r,0,1)              
                    now_left = (u < 0.5)           
                    if (now_left):
                       interv[0] -= (interv[1] - interv[0])
                       g_interv[0]=f(interv[0], arg)
                    else:
                       interv[1] += (interv[1] - interv[0])
                       g_interv[1]=f(interv[1], arg)
                    
                    if p>0:
                       K -= 1
                       if (K<=0):
                           break
     return interv
     
@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)     
cdef bint accept_doubling(double x0, double x1, double y, double w, np.ndarray[ndim=1, dtype=np.float64_t] interv, func_t f, double arg):
     """
     Acceptance test of newly sampled point when the "doubling" procedure has been used to find an 
     interval to sample from (Figure 6, Neal (2003))
     Parameters
         Input:
            x0 ------------ the current point
            x1 ------------- the possible next candidate point
             y ------------ logarithm of the vertical level defining the slice
             w ------------ estimate of the typical size of a slice
     interv[2] ------------ the left and right sides of found interval    
     (*func_t) ------------ routine to compute g(x) = log(f(x))
       Output:
        accept ------------ True/False indicating whether the point is acceptable or not
     """
     cdef double interv1[2]
     cdef double g_interv1[2]
     cdef bint D
     cdef double w11, mid
     w11 = 1.1*w
     interv1[0] = interv[0]
     interv1[1] = interv[1]
     D = False
     while ( (interv1[1] - interv1[0]) > w11):
           mid = 0.5*(interv1[0] + interv1[1])
           if ((x0 < mid) and (x1 >= mid)) or ((x0 >= mid) and (x1 < mid)):
               D = True
           if (x1 < mid):    
               interv1[1] = mid
               g_interv1[1] = f(interv1[1], arg)
           else:
             interv1[0] = mid
             g_interv1[0] = f(interv1[0], arg)
           if (D and (g_interv1[0] < y) and (g_interv1[1] <= y)):
              return False
     return True


@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)     
cdef double shrinkage(double x0, double y, double w, np.ndarray[ndim=1, dtype=np.float64_t] interv, bint doubling, func_t f, double arg):
     """
     Function to sample a point from the interval while skrinking the interval when the sampled point is 
     not acceptable (Figure 5, Neal (2003))
         Input:
            x0 ------------ the current point
             y ------------ logarithm of the vertical level defining the slice
             w ------------ estimate of the typical size of a slice
     interv[2] ------------ the left and right sides of found interval    
     (*func_t) ------------ routine to compute g(x) = log(f(x))
      doubling ------------ 0/1 indicating whether doubling was used to find an interval
       Output:
            x1 ------------- newly sampled point
     """
     cdef double u, gx1, x1
     cdef bint accept
     cdef double L_bar, R_bar
     L_bar=interv[0]
     R_bar=interv[1]
     x1 = L_bar + 0.5*(R_bar - L_bar)  
     cdef int seed = rand()
     #generate random seed
     gsl_rng_set(r, seed)
     
     while True:
           u = unif_interval(r,0,1)
           x1 = L_bar + u*(R_bar - L_bar)
           gx1=f(x1, arg)
           if (doubling):
              accept=accept_doubling(x0, x1, y, w, interv, f , arg)
           else:
              accept=True
           
           if ((gx1 > y) and accept):                 
              break
           if (x1 < x0):
              L_bar = x1
           else:
              R_bar = x1
     return x1
 
 

#***** ----------------------------------------------------------------------------------------- *****
#*****            g(x)=log(f(x)): Compute log-density of a full conditional distribution         *****
#***** ----------------------------------------------------------------------------------------- *****
@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)  
cdef double logUniform(double x, double mu):
     #Log of Uniform distribution with mu as argument 
     if (0<=x<=mu):
        return -log(mu)
     else:
        return -INFINITY
   


@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)  
cdef double logGamma(double x, double beta):
     cdef double alpha = 9.
     return (alpha - 1) * log(x) - beta * x + alpha * log(beta)- log(gsl_sf_gamma(alpha))
   
   
   
@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)     
cdef wrapper make_wrapper(func_t f):
    cdef wrapper W=wrapper()
    W.wrapped=f
    return W

@cython.cdivision(True)     
@cython.boundscheck(False)
@cython.wraparound(False)     
cdef double log_beta(double x, double a):
     #Log of beta distribution with second argument b=1
     cdef double b=1.
     return log(a)+(a-1.)*log(x)+(b-1.)*log(1.-x)     

def slice_sampler(int n_sample,
                  wrapper f, 
                  double a ,
                  int m = 0,
                  int p = 0,
                  double x0_start=0.0, 
                  bint adapt_w=False, 
                  double w_start=0.1,
                  char* interval_method ='doubling'):
     """
     Inputs:
        n_sample ------------ Number of sample points from the given distribution
               f ------------ A log of the function you want to sample and accepts a scalar as an argument (the x) 
        x0_start ------------ An initial value for x
         adapt_w ------------ Whether to adapt w during sampling. Will work in between samplings
         w_start ------------ Starting value for w, necessary if adapting w during sampling
               p ------------ Integer limiting the size of a slice to (2^p)w. If None, then interval can grow without bound
               m ------------ Integer, where maximum size of slice should be mw. If None, then interval can grow without bound
 interval_method ------------ The method for determining the interval at each stage of sampling. Possible values are 'doubling', 'stepping'.
     """
     cdef unicode s= interval_method.decode('UTF-8', 'strict')
     cdef double x0 = x0_start
     cdef double vertical, w, expon   
     cdef double interval_length  
     cdef bint doubling_used=True
     if (s!=u'doubling'):
        doubling_used=False
     cdef double w_n=1.
     cdef vector[double] samples #http://cython.readthedocs.io/en/latest/src/userguide/wrapping_CPlusPlus.html
     w=w_start
     cdef Py_ssize_t i
     cdef np.ndarray[ndim=1, dtype=np.float64_t] interv
     cdef double[::1] vec_view
     cdef int seed = rand()
     #generate random seed
     gsl_rng_set(r, seed)
     for 0<= i <n_sample:
              expon = exponential(r, 1) 
              vertical = f.wrapped(x0, a) - expon
                            
              if (s=='doubling'):
                  vec_view=doubling(x0, vertical, w, p, f.wrapped, a)
                  interv= np.asarray(vec_view)
                  
              elif (s==u'stepping'):
                  vec_view=stepping_out(x0, vertical, w, m, f.wrapped, a)
                  interv= np.asarray(vec_view)
              else:
                  raise ValueError("%s is not an acceptable interval method for slice sampler"%s )
              
              x0=shrinkage(x0, vertical, w, interv, doubling_used, f.wrapped, a)
              samples.push_back(x0) 
              
              if adapt_w:
                 interval_length=interv[1]-interv[0]
                 w=pow(w,w_n/(w_n+1))*pow(interval_length/2.,1./(w_n+1.))
                 w_n+=1.
              
     return samples    

def run(int n_sample,               
        double x0_start = 0., 
        double w_start  = 2.5, 
        double a =5.):
    wrap_f=make_wrapper(log_beta)    
    return slice_sampler(n_sample, wrap_f, a, x0_start=x0_start, w_start=w_start)